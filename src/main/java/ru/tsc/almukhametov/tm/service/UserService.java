package ru.tsc.almukhametov.tm.service;

import ru.tsc.almukhametov.tm.api.repository.IUserRepository;
import ru.tsc.almukhametov.tm.api.service.IUserService;
import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.exception.empty.*;
import ru.tsc.almukhametov.tm.exception.system.EmailExistsException;
import ru.tsc.almukhametov.tm.exception.system.LoginExistsException;
import ru.tsc.almukhametov.tm.exception.system.UserNotFoundException;
import ru.tsc.almukhametov.tm.model.User;
import ru.tsc.almukhametov.tm.util.HashUtil;

import java.util.Optional;

public final class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public User create(String login, String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USER);
        userRepository.add(user);
        return user;
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new LoginExistsException(login);
        if (isEmailExists(email)) throw new EmailExistsException(email);
        final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        repository.add(user);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        repository.add(user);
        return user;
    }

    @Override
    public Optional<User> findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean isLoginExists(final String login) {
        return userRepository.findByLogin(login).isPresent();
    }

    @Override
    public Optional<User> findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean isEmailExists(final String email) {
        return userRepository.findByEmail(email).isPresent();
    }

    @Override
    public Optional<User> removeUser(final User user) {
        if (user == null) return Optional.empty();
        return userRepository.removeUser(user);
    }

    @Override
    public Optional<User> removeUserById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeUserById(id);
    }

    @Override
    public Optional<User> removeUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeUserByLogin(login);
    }

    @Override
    public User updateUser(final String userId, final String firstName, String lastName, String middleName) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final Optional<User> user = findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        user.get().setFirstName(firstName);
        user.get().setLastName(lastName);
        user.get().setMiddleName(middleName);
        return user.get();
    }

    @Override
    public User setPassword(final String userId, final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final Optional<User> user = findById(userId);
        if (!user.isPresent()) throw new EmptyIdException();
        user.get().setPasswordHash(HashUtil.salt(password));
        return user.get();
    }

    @Override
    public User setRole(final String userId, final Role role) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (role == null) throw new EmptyPasswordException();
        final Optional<User> user = findById(userId);
        if (!user.isPresent()) throw new EmptyIdException();
        user.get().setRole(role);
        return user.get();
    }

    @Override
    public User lockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final Optional<User> user = userRepository.findByLogin(login);
        if (!user.isPresent()) throw new EmptyIdException();
        user.get().setLocked(true);
        return user.get();
    }

    @Override
    public User unlockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final Optional<User> user = userRepository.findByLogin(login);
        if (!user.isPresent()) throw new EmptyIdException();
        user.get().setLocked(false);
        return user.get();
    }

}