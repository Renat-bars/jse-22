package ru.tsc.almukhametov.tm.service;

import ru.tsc.almukhametov.tm.api.repository.ITaskRepository;
import ru.tsc.almukhametov.tm.api.service.ITaskService;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.exception.empty.*;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.exception.system.AccessDeniedException;
import ru.tsc.almukhametov.tm.model.Task;

import java.util.Optional;

public final class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
    }

    @Override
    public Optional<Task> findByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(userId, name);
    }

    @Override
    public Optional<Task> removeByName(final String userId, String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeByName(userId, name);
    }

    @Override
    public Task updateById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Optional<Task> task = taskRepository.findById(userId, id);
        task.get().setName(name);
        task.get().setDescription(description);
        return task.get();
    }

    @Override
    public Task updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new EmptyIdException();
        if (taskRepository.getSize() < index - 1) throw new TaskNotFoundException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Optional<Task> task = taskRepository.findByIndex(userId, index);
        task.get().setName(name);
        task.get().setDescription(description);
        return task.get();
    }

    @Override
    public Task startById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Optional<Task> task = taskRepository.findById(userId, id);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.get().setStatus(Status.IN_PROGRESS);
        return task.get();
    }

    @Override
    public Task startByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (taskRepository.getSize() < index - 1) throw new TaskNotFoundException();
        final Optional<Task> task = taskRepository.findByIndex(userId, index);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.get().setStatus(Status.IN_PROGRESS);
        return task.get();
    }

    @Override
    public Task startByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Optional<Task> task = taskRepository.findByName(userId, name);
        if (!task.isPresent()) return null;
        task.get().setStatus(Status.IN_PROGRESS);
        return task.get();
    }

    @Override
    public Task finishById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Optional<Task> task = taskRepository.findById(userId, id);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.get().setStatus(Status.COMPLETED);
        return task.get();
    }

    @Override
    public Task finishByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (taskRepository.getSize() < index - 1) throw new TaskNotFoundException();
        final Optional<Task> task = taskRepository.findByIndex(userId, index);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.get().setStatus(Status.COMPLETED);
        return task.get();
    }

    @Override
    public Task finishByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Optional<Task> task = taskRepository.findById(userId, name);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.get().setStatus(Status.COMPLETED);
        return task.get();
    }

    @Override
    public Task changeTaskStatusById(final String userId, final String id, final Status status) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        return taskRepository.changeTaskStatusById(userId, id, status);
    }

    @Override
    public Task changeTaskStatusByIndex(final String userId, final Integer index, final Status status) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (taskRepository.getSize() < index - 1) throw new TaskNotFoundException();
        if (status == null) throw new EmptyStatusException();
        return taskRepository.changeTaskStatusByIndex(userId, index, status);
    }

    @Override
    public Task changeTaskStatusByName(final String userId, final String name, final Status status) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        return taskRepository.changeTaskStatusByName(userId, name, status);
    }

}
