package ru.tsc.almukhametov.tm.service;

import ru.tsc.almukhametov.tm.api.repository.IAuthenticationRepository;
import ru.tsc.almukhametov.tm.api.service.IAuthenticationService;
import ru.tsc.almukhametov.tm.api.service.IUserService;
import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.exception.empty.EmptyLoginException;
import ru.tsc.almukhametov.tm.exception.empty.EmptyPasswordException;
import ru.tsc.almukhametov.tm.exception.system.AccessDeniedException;
import ru.tsc.almukhametov.tm.model.User;
import ru.tsc.almukhametov.tm.util.HashUtil;

import java.util.Optional;

public final class AuthenticationService implements IAuthenticationService {

    private final IUserService userService;
    private IAuthenticationRepository authenticationRepository;

    public AuthenticationService(final IUserService userService) {
        this.authenticationRepository = null;
        this.userService = userService;
    }

    @Override
    public Optional<User> getCurrentUserId() {
        final String userId = authenticationRepository.getCurrentUserId();
        if (userId == null) throw new AccessDeniedException();
        return userService.findById(userId);
    }

    @Override
    public void setCurrentUserId(String userId) {
        authenticationRepository.setCurrentUserId(userId);
    }

    @Override
    public void checkRoles(Role... roles) {
        if (roles == null || roles.length == 0) return;
        final Optional<User> user = getCurrentUserId();
        if (!user.isPresent()) throw new AccessDeniedException();
        final Role role = user.get().getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item : roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

    @Override
    public boolean isAuthentication() {
        final String currentUserId = authenticationRepository.getCurrentUserId();
        return !(currentUserId == null || currentUserId.isEmpty());
    }

    @Override
    public void logout() {
        if (isAuthentication()) throw new AccessDeniedException();
        setCurrentUserId(null);
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final Optional<User> user = userService.findByLogin(login);
        if (!user.isPresent()) throw new AccessDeniedException();
        if (user.get().getLocked()) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.get().getPasswordHash())) throw new AccessDeniedException();
        setCurrentUserId(user.get().getId());
    }

    @Override
    public void registry(final String login, final String password, final String email) {
        userService.create(login, password, email);
    }

}
