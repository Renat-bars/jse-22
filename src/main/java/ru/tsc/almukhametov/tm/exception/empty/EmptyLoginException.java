package ru.tsc.almukhametov.tm.exception.empty;

import ru.tsc.almukhametov.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error, login is empty");
    }

}
