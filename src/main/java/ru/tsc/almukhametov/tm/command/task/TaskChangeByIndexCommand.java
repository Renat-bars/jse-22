package ru.tsc.almukhametov.tm.command.task;

import ru.tsc.almukhametov.tm.command.AbstractTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.model.Task;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeByIndexCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return TerminalConst.TASK_CHANGE_STATUS_BY_INDEX;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.TASK_CHANGE_STATUS_BY_INDEX;
    }

    @Override
    public void execute() {
        final String userId = String.valueOf(serviceLocator.getAuthenticationService().getCurrentUserId());
        System.out.println("Enter Index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter Task Status");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = serviceLocator.getTaskService().changeTaskStatusByIndex(userId, index, status);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
