package ru.tsc.almukhametov.tm.command.project;

import ru.tsc.almukhametov.tm.command.AbstractProjectCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.exception.empty.EmptyIdException;
import ru.tsc.almukhametov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return TerminalConst.PROJECT_UPDATE_BY_ID;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.PROJECT_UPDATE_BY_ID;
    }

    @Override
    public void execute() {
        final String userId = String.valueOf(serviceLocator.getAuthenticationService().getCurrentUserId());
        System.out.println("Enter Id");
        final String id = TerminalUtil.nextLine();
        if (!serviceLocator.getProjectService().existById(userId, id)) throw new EmptyIdException();
        System.out.println("Enter Name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = serviceLocator.getProjectService().updateById(userId, id, name, description);
        if (projectUpdated == null) throw new ProjectNotFoundException();
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
