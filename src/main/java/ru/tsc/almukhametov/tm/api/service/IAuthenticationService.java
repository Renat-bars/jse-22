package ru.tsc.almukhametov.tm.api.service;

import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.model.User;

import java.util.Optional;

public interface IAuthenticationService {

    Optional<User> getCurrentUserId();

    void setCurrentUserId(String userId);

    void checkRoles(Role... roles);

    boolean isAuthentication();

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

}
