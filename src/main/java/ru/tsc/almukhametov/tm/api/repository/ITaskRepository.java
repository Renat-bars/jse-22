package ru.tsc.almukhametov.tm.api.repository;

import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.model.Task;

import java.util.List;
import java.util.Optional;

public interface ITaskRepository extends IOwnerRepository<Task> {

    Optional<Task> findByName(String userId, String name);

    Optional<Task> removeByName(String userId, String name);

    Task startById(String userId, String id);

    Task startByIndex(String userId, Integer id);

    Task startByName(String userId, String name);

    Task finishById(String userId, String id);

    Task finishByIndex(String userId, Integer index);

    Task finishByName(String userId, String name);

    Task changeTaskStatusById(String userId, String id, Status status);

    Task changeTaskStatusByIndex(String userId, Integer index, Status status);

    Task changeTaskStatusByName(String userId, String name, Status status);

    List<Task> findAllTaskByProjectId(String userId, String projectId);

    List<Task> removeAllTaskByProjectId(String userId, String projectId);

    Task bindTaskById(String userId, String projectId, String taskId);

    Task unbindTaskById(String userId, String projectId, String taskId);

}
