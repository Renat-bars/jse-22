package ru.tsc.almukhametov.tm.api.service;

public interface IServiceLocator {

    ITaskService getTaskService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ICommandService getCommandService();

    IAuthenticationService getAuthenticationService();

    IUserService getUserService();

}
