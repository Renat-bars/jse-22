package ru.tsc.almukhametov.tm.api.service;

import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.model.Task;

import java.util.List;
import java.util.Optional;

public interface IProjectTaskService {

    List<Task> findAllTaskByProjectId(String userId, String projectId);

    Task bindTaskById(String userId, String projectId, String taskId);

    Task unbindTaskById(String userId, String projectId, String taskId);

    Optional<Project> removeById(String userId, String projectId);

    Optional<Project> removeByIndex(String userId, Integer index);

    Optional<Project> removeByName(String userId, String name);

    void clearProjects(String userId);

}
