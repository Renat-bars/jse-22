package ru.tsc.almukhametov.tm.api.service;

import ru.tsc.almukhametov.tm.api.repository.IOwnerRepository;
import ru.tsc.almukhametov.tm.model.AbstractOwnerEntity;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IOwnerRepository<E> {

}
