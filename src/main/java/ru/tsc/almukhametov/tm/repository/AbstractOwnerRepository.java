package ru.tsc.almukhametov.tm.repository;

import ru.tsc.almukhametov.tm.api.repository.IOwnerRepository;
import ru.tsc.almukhametov.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    protected Predicate<E> predicateByUserId(final String userId) {
        return e -> userId.equals(e.getUserId());
    }

    protected Predicate<E> predicateById(final String id) {
        return e -> id.equals(e.getId());
    }

    public E add(final String userId, final E entity) {
        entity.setUserId(userId);
        list.add(entity);
        return entity;
    }

    public void remove(final String userId, final E entity) {
        if (!userId.equals(entity.getUserId())) return;
        list.remove(entity);
    }

    public List<E> findAll(final String userId) {
        return list.stream()
                .filter(predicateByUserId(userId))
                .collect(Collectors.toList());
    }

    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        return list.stream()
                .filter(predicateByUserId(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    public void clear(final String userId) {
        final List<E> list = findAll(userId);
        this.list.removeAll(list);
    }

    public Optional<E> findById(final String userId, final String id) {
        return list.stream()
                .filter(predicateByUserId(userId))
                .filter(predicateById(id))
                .findFirst();
    }

    public Optional<E> findByIndex(final String userId, final Integer index) {
        return list.stream()
                .filter(e -> e.getUserId().equals(userId))
                .skip(index)
                .findFirst();
    }

    public Optional<E> removeById(final String userId, final String id) {
        final Optional<E> entity = findById(userId, id);
        entity.ifPresent(this::remove);
        return entity;
    }

    public Optional<E> removeByIndex(final String userId, final Integer index) {
        final Optional<E> entity = findByIndex(userId, index);
        entity.ifPresent(this::remove);
        return entity;
    }

    public boolean existById(final String userId, final String id) {
        final Optional<E> entity = findById(userId, id);
        return entity.isPresent();
    }

    public boolean existByIndex(final String userId, final int index) {
        final List<E> list = findAll(userId);
        if (index < 0) return false;
        return index < list.size();
    }

}
