package ru.tsc.almukhametov.tm.repository;

import ru.tsc.almukhametov.tm.api.IRepository;
import ru.tsc.almukhametov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    public E add(final E entity) {
        list.add(entity);
        return entity;
    }

    public Optional<E> remove(final E entity) {
        list.remove(entity);
        return Optional.ofNullable(entity);
    }

    public List<E> findAll() {
        return list;
    }

    public List<E> findAll(final Comparator<E> comparator) {
        final List<E> entities = new ArrayList<>(this.list);
        entities.sort(comparator);
        return entities;
    }

    public void clear() {
        list.removeAll(list);
    }

    public Optional<E> findById(final String id) {
        return list.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst();
    }

    public Optional<E> findByIndex(final Integer index) {
        return Optional.ofNullable(list.get(index));
    }

    public Optional<E> removeById(final String id) {
        final Optional<E> entity = findById(id);
        entity.ifPresent(this::remove);
        return entity;
    }

    public Optional<E> removeByIndex(final Integer index) {
        final Optional<E> entity = findByIndex(index);
        entity.ifPresent(this::remove);
        return entity;

    }

    public boolean existById(final String id) {
        final Optional<E> entity = findById(id);
        return entity.isPresent();
    }

    public boolean existByIndex(final int index) {
        final List<E> list = findAll();
        if (index < 0) return false;
        return index < list.size();
    }

    public Integer getSize() {
        return list.size();
    }

}
