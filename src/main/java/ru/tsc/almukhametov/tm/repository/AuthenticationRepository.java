package ru.tsc.almukhametov.tm.repository;

import ru.tsc.almukhametov.tm.api.repository.IAuthenticationRepository;

public class AuthenticationRepository implements IAuthenticationRepository {

    private String currentUserId;

    @Override
    public String getCurrentUserId() {
        return currentUserId;
    }

    @Override
    public void setCurrentUserId(String currentUserId) {
        this.currentUserId = currentUserId;
    }
}
