package ru.tsc.almukhametov.tm.repository;

import ru.tsc.almukhametov.tm.api.repository.ITaskRepository;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public Optional<Task> findByName(final String userId, final String name) {
        return Optional.ofNullable(findAll(userId).stream()
                .filter(t -> t.getName().equals(name))
                .findFirst()
                .orElseThrow(TaskNotFoundException::new));
    }

    @Override
    public Optional<Task> removeByName(final String userId, final String name) {
        final Optional<Task> task = (findByName(userId, name));
        task.ifPresent(this::remove);
        return Optional.ofNullable(task.orElseThrow(TaskNotFoundException::new));
    }

    @Override
    public Task startById(final String userId, final String id) {
        final Optional<Task> task = findById(userId, id);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(t -> t.setStatus(Status.IN_PROGRESS));
        return task.orElse(null);
    }

    @Override
    public Task startByIndex(final String userId, final Integer index) {
        final Optional<Task> task = findByIndex(userId, index);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(t -> t.setStatus(Status.IN_PROGRESS));
        return task.orElse(null);
    }

    @Override
    public Task startByName(final String userId, final String name) {
        final Optional<Task> task = findByName(userId, name);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(t -> t.setStatus(Status.IN_PROGRESS));
        return task.orElse(null);
    }

    @Override
    public Task finishById(final String userId, final String id) {
        final Optional<Task> task = findById(userId, id);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(t -> t.setStatus(Status.COMPLETED));
        return task.orElse(null);
    }

    @Override
    public Task finishByIndex(final String userId, final Integer index) {
        final Optional<Task> task = findByIndex(userId, index);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(t -> t.setStatus(Status.COMPLETED));
        return task.orElse(null);
    }

    @Override
    public Task finishByName(final String userId, final String name) {
        final Optional<Task> task = findByName(userId, name);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(t -> t.setStatus(Status.COMPLETED));
        return task.orElse(null);
    }

    @Override
    public Task changeTaskStatusById(final String userId, final String id, final Status status) {
        final Optional<Task> task = findById(userId, id);
        task.ifPresent(t -> t.setStatus(status));
        return task.orElse(null);
    }

    @Override
    public Task changeTaskStatusByIndex(final String userId, final Integer index, final Status status) {
        final Optional<Task> task = findByIndex(userId, index);
        task.ifPresent(t -> t.setStatus(status));
        return task.orElse(null);
    }

    @Override
    public Task changeTaskStatusByName(final String userId, final String name, final Status status) {
        final Optional<Task> task = findByName(userId, name);
        task.ifPresent(t -> t.setStatus(status));
        return task.orElse(null);
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String userId, final String projectId) {
        final List<Task> tasksByProject = new ArrayList<>();
        for (final Task task : list) {
            if (projectId.equals(task.getProjectId()))
                tasksByProject.add(task);
        }
        return tasksByProject;
    }

    @Override
    public List<Task> removeAllTaskByProjectId(final String userId, final String projectId) {
        for (final Task task : list) {
            if (projectId.equals(task.getProjectId()))
                task.setProjectId(null);
        }
        return list;
    }

    @Override
    public Task bindTaskById(final String userId, final String projectId, final String taskId) {
        final Optional<Task> task = findById(userId, taskId);
        task.ifPresent(t -> t.setProjectId(projectId));
        return task.orElse(null);
    }

    @Override
    public Task unbindTaskById(final String userId, final String projectId, final String taskId) {
        final Optional<Task> task = findById(userId, taskId);
        task.ifPresent(t -> t.setProjectId(null));
        return task.orElse(null);
    }

}
